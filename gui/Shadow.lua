--[[
    Embers - A custom ProbablyEngine Combat Priority
    -- Created by Mirakuru

    Released under the GNU General Public License v3.0
]]
shadowGUI = {
    key = "shadowCFG",
    profiles = true,
    title = "Shadow Priest",
    subtitle = "User Settings",
    color = "9C9C9C",
    width = 300,
    height = 450,
    config = {
        {
            type = "texture",
            texture = "Interface\\AddOns\\Probably_Embers\\gui\\media\\shadow.blp",
            center = true,
            width = 256,
            height = 256,
            offset = 90,
            y = 35
        },
        {type = "spacer"},
        {
            type = "header",
            text = "Advanced Settings",
            align = "left"
        },
        {type = "spacer"},
        {
            type = "checkbox",
            default = false,
            text = "Enable Object Supervisor & Manager",
            key = "enableObjects",
            desc = "Needed for advanced priority features, you can still turn it off and other unneeded features later. Only supports FireHack."
        },
        
        {type = "spacer"},{type = "spacer"},{type = "spacer"},
        {
            type = "header",
            text = "Talents & Glyphs",
            align = "left"
        },
        {type = "spacer"},
		{
			type = "checkspin",
			default_check = false,
			default_spin = 25,
			width = 55,
			min = 0,
			max = 100,
			step = 1,
			text = "Desperate Prayer",
			key = "prayer",
			desc = "Use Desperate Prayer when you fall below this health %."
		},
        {type = "spacer"},
        {
            type = "checkbox",
            default = false,
            text = "Body and Soul",
            key = "speed1",
            desc = "Use Power Word: Shield on yourself while moving."
        },
        {type = "spacer"},
        {
            type = "checkbox",
            default = false,
            text = "Angelic Feather",
            key = "speed2",
            desc = "Cast Angelic Feather on yourself while moving."
        },
        {type = "spacer"},
        {
            type = "checkbox",
            default = false,
            text = "Use Speed Booster Outside Combat",
            key = "speed3",
            desc = "Will make use of Body and Soul and Angelic Feather outside of combat."
        },
        {type = "space"},
		{
			type = "dropdown",
			text = "Mindbender",
			key = "mindbender",
			list = {
				{
					text = "On Cooldown",
					key = "mbOnCD"
				},
				{
					text = "Boss Only",
					key = "mbOnBoss"
				},
                {
                    text = "Disabled",
                    key = "noMB"
                }
			},
			desc = "Select how to use Mindbender when the talent is selected.",
			default = "mbOnCD"
		},
        {type = "space"},
		{
			type = "dropdown",
			text = "Void Tendrils",
			key = "tendrils",
			list = {
				{
					text = "Target in Range",
					key = "tRange"
				},
				{
					text = "Any in Range",
					key = "aRange"
				},
                {
                    text = "Disabled",
                    key = "noVT"
                }
			},
			desc = "Select your Void Tendrils behaviour.",
			default = "noVT"
		},
        {type = "space"},
		{
			type = "dropdown",
			text = "Power Infusion",
			key = "infusion",
			list = {
				{
					text = "On Cooldown",
					key = "infOnCD"
				},
				{
					text = "Boss Only",
					key = "infOnBoss"
				},
                {
                    text = "Pool with Other",
                    key = "infPool"
                },
                {
                    text = "Disabled",
                    key = "noPI"
                }
			},
			desc = "Select how to use Power Infusion when the talent is selected.",
			default = "infOnCD"
		},
        {type = "spacer"},
        {
            type = "checkbox",
            default = false,
            text = "Glyph of Reflective Shield",
            key = "refShield",
            desc = "Use Power Word: Shield on cooldown with Glyph of Reflective Shield active."
        },
        {type = "spacer"},
        {
            type = "checkbox",
            default = false,
            text = "Glyph of Fade",
            key = "fade",
            desc = "Use Fade on cooldown with Glyph of Fade active."
        },
        {type = "spacer"},{type = "spacer"},{type = "spacer"},
        {
            type = "header",
            text = "Shadow General",
            align = "left"
        },
        {type = "spacer"},
        {
            type = "checkbox",
            default = true,
            text = "Power Word: Fortitude",
            key = "pwf",
            desc = "Keep Power Word: Fortitude buffed."
        },
        {type = "spacer"},
        {
            type = "checkbox",
            default = true,
            text = "Shadowform",
            key = "form",
            desc = "Keep Shadowform active."
        },
        {type = "spacer"},
		{
			type = "checkspin",
			default_check = false,
			default_spin = 15,
			width = 55,
			min = 0,
			max = 100,
			step = 1,
			text = "Dispersion",
			key = "disp1",
			desc = "Use Dispersion when you fall below or to this health %."
		},
		{
			type = "checkspin",
			default_check = false,
			default_spin = 40,
			width = 55,
			min = 0,
			max = 100,
			step = 1,
			text = "Deactivate Dispersion",
			key = "disp2",
			desc = "Deactivate Dispersion when you reach this health %."
		},
        {type = "spacer"},
		{
			type = "checkspin",
			default_check = false,
			default_spin = 75,
			width = 55,
			min = 0,
			max = 100,
			step = 1,
			text = "Gift of the Naaru",
			key = "naaru",
			desc = "Use Gift of the Naaru when you fall below this health %."
		},
        {type = "space"},
		{
			type = "dropdown",
			text = "Shadowfiend",
			key = "shadowfiend",
			list = {
				{
					text = "On Cooldown",
					key = "sfOnCD"
				},
				{
					text = "Boss Only",
					key = "sfOnBoss"
				},
                {
                    text = "Disabled",
                    key = "noSF"
                }
			},
			desc = "Select how to use Shadowfiend while in combat.",
			default = "sfOnCD"
		},
        {type = "space"},
		{
			type = "dropdown",
			text = "Draenic Intellect Potion",
			key = "dpsPot",
			list = {
				{
					text = "On Cooldown",
					key = "potOnCD"
				},
				{
					text = "On Heroism",
					key = "potHero"
				},
				{
					text = "Boss @ 20% HP",
					key = "potBoss20"
				},
                {
                    text = "Disabled",
                    key = "noPot"
                }
			},
			desc = "Select how to use Draenic Intellect Potion on Boss Encounters.",
			default = "potHero"
		},
        {type = "space"},
		{
			type = "dropdown",
			text = "Trinkets",
			key = "trinket",
			list = {
				{
					text = "On Cooldown",
					key = "trinketCD"
				},
				{
					text = "Boss Only",
					key = "trinketBoss"
				},
				{
					text = "Pool with Other",
					key = "trinketPool"
				},
                {
                    text = "Disabled",
                    key = "noTrinket"
                }
			},
			desc = "Select how to use On-Use Trinkets while in combat.",
			default = "trinketPool"
		},
        {type = "space"},
		{
			type = "dropdown",
			text = "Racial Abilities",
			key = "racials",
			list = {
				{
					text = "On Cooldown",
					key = "racialCD"
				},
				{
					text = "Boss Only",
					key = "racialBoss"
				},
				{
					text = "Pool with Other",
					key = "racialPool"
				},
                {
                    text = "Disabled",
                    key = "noRacial"
                }
			},
			desc = "Select how to use Berserking and Blood Fury while in combat.",
			default = "racialPool"
		},
        {type = "spacer"},
        {
            type = "checkbox",
            default = false,
            text = "Vampiric Embrace with Devouring Plague",
            key = "veDP",
            desc = "Use Vampiric Embrace together with Devouring Plague."
        },
        {type = "spacer"},
		{
			type = "checkspin",
			default_check = false,
			default_spin = 85,
			width = 55,
			min = 0,
			max = 100,
			step = 1,
			text = "Vampiric Embrace",
			key = "ve",
			desc = "Use Vampiric Embrace when total Party/Raid health falls below this health %."
		}
    }
}