--[[
    Embers - A custom ProbablyEngine Combat Priority
    -- Created by Mirakuru

    Released under the GNU General Public License v3.0
]]
-- local round
local round = Embers.round

-- easier setting function
local fetch = function(setting)
    local key = ProbablyEngine.interface.fetchKey('shadowCFG',setting)
    return key
end

-- Cache GetSpellInfo
local spellcache = setmetatable({}, {__index=function(t,v) local a = {GetSpellInfo(v)} if GetSpellInfo(v) then t[v] = a end return a end})
local function GetSpellInfo(a)
    return unpack(spellcache[a])
end

-- Dynamic eval PE conditions
local function eval(condition, spell)
	if not condition then return false end
	return ProbablyEngine.dsl.parse(condition, spell or '')
end


local onLoad = function()
    -- Overload PE functions
    Embers.overloads()
    
    -- Settings
    Embers.settings(shadowGUI)
    Embers.settings(shadowGUI)
    
    -- Settings Toggle
    ProbablyEngine.toggle.create('Settings', 'Interface\\Icons\\trade_engineering.png"', 'Settings', 'Toggle Settings', (function() Embers.settings(shadowGUI) end))
end

local notInCombat = {
    -- Shadowform
    {"15473", {"player.stance = 0", (function() return fetch('form') end)}},
    
    -- Power Word: Fortitude
    {"21562", {"!player.buffs.stamina", (function() return fetch('pwf') end)}},
    
    -- Speed Boosters
    {{
            -- Body and Soul
            {"17", {"talent(2, 1)", "!player.debuff(6788)", "player.moving", (function() return fetch("speed1") end)}},
            
            -- Angelic Feather
            {"!121536", {
                    "talent(2, 2)",
                    "player.moving",
                    "!player.buff(121557)",
                    "player.spell(121536).charges >= 1",
                    (function() return fetch("speed2") end)},
                "player.ground"}
        }, (function() return fetch("speed3") end)}
}

local combatPriority = {
    -- ★★★★★ Sustain and Survivability ★★★★★
    -- Shadowform
    {"15473", {"player.stance = 0", (function() return fetch('form') end)}},
    
    -- Power Word: Fortitude
    {"21562", {"!player.buffs.stamina", (function() return fetch('pwf') end)}},
    
    -- Desperate Prayer
    {"19236", {"talent(1,1)", (function() return fetch("prayer_check") and eval("player.health <= "..fetch("prayer_spin")) end)}},
    
    -- Glyph of Reflective Shield
    {"17", {"player.glyph(33202)", "!player.debuff(6788)", (function() return fetch("refShield") end)}},
    
    -- Glyph of Fade
    {"586", {"player.glyph(55684)", (function() return fetch("fade") end)}},
    
    -- Body and Soul
    {"17", {"talent(2, 1)", "!player.debuff(6788)", "player.moving", (function() return fetch("speed1") end)}},
    
    -- Angelic Feather
    {"121536", {
            "talent(2, 2)",
            "player.moving",
            "!player.buff(121557)",
            "player.spell(121536).charges >= 1",
            (function() return fetch("speed2") end)},
        "player.ground"},
    
    -- Void Tendrils
    {"108920", {"talent(4, 1)", (function()
                    if fetch("tendrils") == "noVT" then return false end
                    if fetch("tendrils") == "tRange" then return eval("target.range <= 8") end
                    if fetch("tendrils") == "aRange" then return eval("player.area(8).enemies > 0") end
                    return false
                end)}},
    
    -- Gift of the Naaru
    {"59544", (function()
                if fetch("naaru_check") and eval("player.health <= "..fetch("naaru_spin")) then return true end
                return false
            end)},
    
    -- Vampiric Embrace
    {{
            {"15286", (function() return eval("raid.health <= "..fetch("ve_spin")) end)},
            {"2944", {"player.shadoworbs >= 3", "player.buff(15286).duration >= 5", (function() return fetch("veDP") end)}}
        }, (function() return fetch("ve_check") end)},
    
    -- Dispersion
    {{
            {"/cancelaura "..GetSpellInfo(47585),
                {"player.buff(47585)", (function()
                            if fetch("disp2_spin") < fetch("disp1_spin") then return false end
                            return fetch("disp2_check") and eval("player.health >= "..fetch("disp2_spin")) end)}},
            {"!47585", (function() return fetch("disp1_check") and eval("player.health <= "..fetch("disp1_spin")) end)}
        }},
    
    
    -- ★★★★★ Cooldownsm Trinkets and Potions ★★★★★
    {{
            -- Mindbender / Shadowfiend
            {"34433", {"talent(3, 2)", (function()
                            if fetch("mindbender") == "noMB" then return false end
                            if fetch("mindbender") == "mbOnCD" then return true end
                            if fetch("mindbender") == "mbOnBoss" then return eval("target.isBoss") end
                        end)}},
            
            -- Power Infusion
            {"10060", {"talent(5, 2)", (function()
                            if fetch("infusion") == "noPI" then return false end
                            if fetch("infusion") == "infOnCD" then return true end
                            if fetch("infusion") == "infOnBoss" then return eval("target.isBoss") end
                            if fetch("infusion") == "infPool" then
                                -- Pooling together with Shadowfiend/Mindbender and possible trinkets
                                return (eval("lastcast(34433)") or eval("lastcast(132604)") or eval("player.any.buff >= 1"))
                            end
                        end)}},
            
            -- Trinkets
            {{
                    {"#trinket1"},
                    {"#trinket2"}
            }, (function()
                        if fetch("trinket") == "noTrinket" then return false end
                        if fetch("trinket") == "trinketCD" then return true end
                        if fetch("trinket") == "trinketBoss" then return eval("target.isBoss") end
                        if fetch("trinket") == "trinketPool" then
                            return (eval("lastcast(34433)") or eval("lastcast(132604)") or eval("player.any.buff >= 1"))
                        end
                    end)},
            
            -- Racial Abilities
        }, "modifier.cooldowns"},
    
    
    
    
    
    
    
    
    
    
    
    
    -- ★★★★★ Auspicious Spirits Priority ★★★★★
    -- Sync to GCD
    {"pause", (function()if UnitCastingInfo("player") then return true end end)},
    
    -- Devouring Plague
    {"!2944", {"player.shadoworbs = 5", "!target.debuff(158831)"}},
    {"!2944", {"player.shadoworbs = 5", "@Embers.objectSupervisor(2944)"}},
    {"!2944", {"player.shadoworbs = 5"}},
    {"!2944", {"player.shadoworbs = 5", "@Embers.objectSupervisor(2944, nil, true)"}},
    
    -- Shadow Word: Death
    {"!32379", {"target.health < 20", "player.shadoworbs < 5"}},
    {"!32379", "@Embers.objectSupervisor(32379)"},
    
    -- Devouring Plague
    {{
        {{
            {"!2944", "talent(3,1)"},
            {"!2944", "player.tier17 >= 4"}
        }, {"player.shadoworbs = 5", "!target.debuff(158831)"}},
        
        {"!2944", (function()
            local gcd = round((1.5/((GetHaste()/100)+1)),2)
            return (eval("player.buff(167254).duration > "..(gcd*0.7)) and eval("player.buff(167254).duration < "..gcd))
        end)},
        
        {{
            {"!2944", {"player.tier17 < 2", "player.tier18 < 4", (function()
                local gcd = round((1.5/((GetHaste()/100)+1)),2)
                return eval("player.spell(8092).cooldown < "..gcd)
            end)}},
            
            {"!2944", {"target.health < 20", (function()
                local gcd = round((1.5/((GetHaste()/100)+1)),2)
                return eval("player.spell(32379).cooldown < "..gcd)
            end)}},
        }, {"player.shadoworbs >= 4", "!target.debuff(158831)", "talent(3,1)"}},
        
        {{
            {"!2944", {"player.tier17 < 2", "player.tier18 < 4", "!talent(3, 2)", (function()
                local gcd = round((1.5/((GetHaste()/100)+1)),2)
                return eval("player.spell(8092).cooldown < "..gcd)
            end)}},
            
            {"!2944", {"target.health < 20", (function()
                local gcd = round((1.5/((GetHaste()/100)+1)),2)
                return eval("player.spell(32379).cooldown < "..gcd)
            end)}},
        }, "player.shadoworbs >= 4"},
        
        {"!2944", {"player.tier18 >= 4", "talent(3, 2)", "player.buff(188779)"}}
    }, {"player.shadoworbs >= 3"}},
    
    -- Mind Blast
    {"!8092", {"player.enemies < 4", "!player.moving"}},
    
    -- Shadow Word: Pain
    {"589", {"target.debuff(589).duration < 5.4", "target.ttd > 13"}},
    {"589", "@Embers.objectSupervisor(589)"},
    
    -- Mind Blast
    {"!8092", "!player.moving"},
    
    -- T18 Trinket Gaming
    -- Searing Insanity
    
    -- Insanity
    {"129197", {"player.buff(132573).duration > 0.4", "player.enemies <= 1", "!player.moving", "player.spell(8092).cooldown >= 0.5"}},
    
    -- Halo
    -- Cascade
    -- Divine Star
    
    -- Vampiric Touch
    {"34914", {"player.enemies <= 5", "!player.moving", (function()
        local castTime = round((1.5/((GetHaste()/100)+1)),2)
        return (eval("target.debuff(34914).duration < "..(15*0.3+castTime)) and eval("target.ttd > "..(15*0.75+castTime)))
    end)}},
    {"34914", "@Embers.objectSupervisor(34914)"},
    
    -- Devouring Plague
    {"2944", {"player.shadoworbs >= 3", "target.debuff(158831).duration < 1"}},
    
    -- Mind Spike
    {"73510", {"player.buff(87160).count >= 1", "player.enemies <= 4", "player.tier18 >= 4", "player.buff(188779)"}},
    {"73510", {"player.buff(87160).count = 3", "player.enemies <= 4"}},
    
    -- Halo
    -- Cascade
    -- Divine Star
    
    -- Shadow Word: Pain
    {"589", "player.moving"},
    
    -- Pause for Shadow Word: Death & Mind Blast cooldown under GCD
    {"pause", {"player.spell(32379).cooldown < 0.5", "player.enemies <= 1", "target.health < 20"}},
    {"pause", {"player.spell(8092).cooldown < 0.5", "player.enemies <= 1"}},
    
    -- Mind Spike
    {{
        {"73510", "player.tier18 < 4"},
        {"73510", {"player.tier18 >= 4", "player.buff(188779)"}},
        {"73510", {"player.tier18 >= 4", "player.spell(34433).cooldown > 13", "player.buff(87160).duration < 4"}}
    }, {"player.buff(87160)", "player.enemies <= 5"}},
    
    -- Divine Star
    -- Mind Sear
    
    -- Shadow Word: Pain
    {"589", {"target.debuff(589).duration < 16.2", "target.ttd > 13", "player.enemies >= 3"}},
    {"589", "@Embers.objectSupervisor(589)"},
    {"589", {"player.shadoworbs >= 2", "target.debuff(589).duration < 9", "target.ttd > 13", "talent(3, 3)"}},
    
    -- Vampiric Touch
    {"34914", {"player.shadoworbs >= 2", "target.debuff(34914).duration < 7", "target.ttd > 13", "talent(3, 3)", "!player.moving"}},
    
    -- Mind Flay
    {"15407", "!player.moving"}
}

ProbablyEngine.rotation.register_custom(258, "|cffffffff[|cffFF5800Mirakuru's Ember|cffffffff] Shadow", combatPriority, notInCombat, onLoad)